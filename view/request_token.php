<form form-request-token novalidate>
	<input class="form-input" type="text" name="username" ng-model="username" placeholder="Enter IITK username"/>
	<img class="img-captcha" ng-src="{{captcha_src}}">
	<input class="form-input" type="text" name="captcha" ng-model="captcha" placeholder="Enter captcha"/>
	<input class="form-submit" type="submit" value="Submit"/>
</form>