<form novalidate>
	<input class="form-input" type="text" name="username" ng-model="username" placeholder="Enter IITK username"/>
	<input class="form-input" type="text" name="token" ng-model="token" placeholder="Enter token"/>
	<input class="form-file" type="file" nv-file-select uploader="uploader" accept="image/*" value="Select Photo" />
	<div class="form-thumb" ng-repeat="item in uploader.queue">
		Preview
		<div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 100 }"></div>
		<div class="progress" style="margin-bottom: 0;">
		    <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }">{{item.progress}}%</div>
		</div>
	</div>
	<input class="form-submit" type="submit" value="Upload" ng-click="uploader.uploadAll()" ng-show="upload_flag"/>
</form>
