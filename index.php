<!DOCTYPE html>
<html>
<head>
	<title>Change Profile Picture | Student Search IITK</title>
	<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body ng-app="student-search-photo">
	<div class="container">
		<div class="container-head">
			<h1>Change your profile picture in just two simple steps</h1>
			<div style="padding-top:10px;">
				<a class="btn-steps" ng-href="#/">I - Request Token</a>
				<a class="btn-steps" ng-href="#/upload_photo">II - Upload Photo</a>
			</div>
		</div>
		<div class="container-body">
			<div ng-view></div>
		</div>
		<div class="container-footer">{{message}}</div>
	</div>	
	<footer>
		<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/angular.min.js"></script>
		<script type="text/javascript" src="js/angular-route.js"></script>
		<script type="text/javascript" src="js/angular-file-upload.min.js"></script>
		<script type="text/javascript" src="js/app.js"></script>
	</footer>
</body>
</html>