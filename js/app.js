/*
    Author : Himanshu Choudhary
    Email : himanshuchoudhary@live.com
    Git : https://bitbucket.org/himanshuchoudhary/

*/

var app = angular.module('student-search-photo',['ngRoute','angularFileUpload']);

app.config(['$routeProvider', '$locationProvider',function($routeProvider, $locationProvider){
    $routeProvider
        .when('/upload_photo',{
            templateUrl : 'view/upload_photo.php',
            controller : 'uploadPhotoController'
        })
        .otherwise({
            templateUrl : 'view/request_token.php',
            controller : 'requestTokenController'
        });

    $locationProvider.html5Mode({
        enabled : false,
        requireBase : false
    });
}]);

app.directive('formRequestToken',['$rootScope','$http','$timeout', function($rootScope,$http,$timeout) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, ele, attr) {
            scope.username = '';
            scope.captcha = '';
            ele.on('submit', function(){
                if (scope.username != '' && scope.captcha != ''){
                    var params = {
                        username : scope.username,
                        captcha : scope.captcha
                    };
                    $rootScope.message = 'Loading..';
                    $http.post('php/get_token.php',params).success(function(response){
                        if(response.success == true){
                            $http.get('php/get_captcha.php').success(function(response){
                                scope.captcha_src = response.captcha_src;
                                scope.captcha = '';
                            });
                        }
                        $rootScope.message = response.message;
                    }).error(function(){
                        $rootScope.message = 'Some error occured. Please try again.';
                    });
                }
                else {
                    $timeout(function() {
                        $rootScope.message = 'Invalid inputs';
                    });
                }            
            });
        }
    };
}]);

app.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);

app.controller('requestTokenController',['$rootScope','$scope','$http', function($rootScope,$scope,$http) {
    $rootScope.message = '';

    $http.get('php/get_captcha.php').success(function(response){
        $scope.captcha_src = response.captcha_src;
    });
    $scope.captcha = null;
}]);

app.controller('uploadPhotoController',['$rootScope','$scope','$http','FileUploader', function($rootScope,$scope,$http,FileUploader) {
    $scope.captcha = null;
    $scope.upload_flag = true;
    $rootScope.message = '';

    $scope.uploader = new FileUploader({
        url: 'php/upload_photo.php'
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.uploader.onBeforeUploadItem = function(item) {
        item.formData.push({
            username: $scope.username,
            token: $scope.token
        });
    };

    $scope.uploader.onCompleteItem = function(item, response, status, headers) {
        $scope.upload_flag = false;
        $rootScope.message = response.message;
    };
}]);