<?php
	/*
	    Author : Himanshu Choudhary
	    Email : himanshuchoudhary@live.com
	    Git : https://bitbucket.org/himanshuchoudhary/

	*/
	include 'config/db_config.php';

	$response = [];

	if(!empty( $_FILES)){
		
		$username = $_POST['username'];
		$token = $_POST['token'];
		
		$tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
		$uploadPath = dirname(dirname( __FILE__)) . DIRECTORY_SEPARATOR . 'photos'.DIRECTORY_SEPARATOR.$username.'.jpg';

		$query = $db->query("SELECT * FROM tokens WHERE username='$username' AND token='$token'");
		$count = $query->num_rows;

		if($count > 0){
			$db->query("DELETE FROM tokens WHERE token='$token'");
			move_uploaded_file( $tempPath, $uploadPath);
			$response = array(
				'success' => TRUE,
				'data' => NULL,
				'message' => 'Photo successfully uploaded.'
	 		);
		}
		else {
			$response = array(
				'success' => FALSE,
				'data' => NULL,
				'message' => 'Invalid token.'
	 		);
		}
	}
	else {
		$response = array(
			'success' => FALSE,
			'data' => NULL,
			'message' => 'No image received.'
 		);
	}
	echo json_encode($response);
?>