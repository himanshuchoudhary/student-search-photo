<?php
	/*
	    Author : Himanshu Choudhary
	    Email : himanshuchoudhary@live.com
	    Git : https://bitbucket.org/himanshuchoudhary/

	*/
	$_POST = json_decode(file_get_contents('php://input'), true);

	include 'validate_captcha.php';
	include 'generate_token.php';
	include 'config/db_config.php';
	$response = [];

	if(matchCaptcha($_POST['captcha'])){
		$token = getToken(10);
		$username = $_POST['username'];

		$to = $username."@iitk.ac.in";
		$subject = "Token to change display picture";
		$txt = "Use this token to change your picture : ".$token;
		$headers = "From: admin@choudhary.esy.es";

		if(mail($to,$subject,$txt,$headers)){
			$query = "INSERT INTO tokens (username, token) VALUES ('$username','$token') ON DUPLICATE KEY UPDATE token='$token'";
			if($db->query($query)){
				$response['success'] = TRUE;
				$response['data'] = NULL;
				$response['message'] = 'Token has been sent to your webmail.';
			}
			else {
				$response['success'] = FALSE;
				$response['data'] = NULL;
				$response['message'] = 'Some error occured. Please try again.';
			}
		}
		else {
			$response['success'] = FALSE;
			$response['data'] = NULL;
			$response['message'] = 'Invalid email.';
		}
	}
	else {
		$response['success'] = FALSE;
		$response['data'] = NULL;
		$response['message'] = 'Captcha did not match.';
	}

	echo json_encode($response);
?>