<?php
	/*
	    Author : Himanshu Choudhary
	    Email : himanshuchoudhary@live.com
	    Git : https://bitbucket.org/himanshuchoudhary/

	*/
	session_start();
	$_SESSION = array();

	include("lib/captcha/simple-php-captcha.php");
	$_SESSION['captcha'] = simple_php_captcha();

	$response = array(
		'captcha_src' => $_SESSION['captcha']['image_src']
	);

	echo json_encode($response);
?>